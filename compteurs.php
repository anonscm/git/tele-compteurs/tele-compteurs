<!DOCTYPE html>
<html>
	<head>
		<title>Compteurs - setting</title>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body>
<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

try{
	$pdo = new PDO('sqlite:'.dirname(__FILE__).'/compteur.db');
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
	//$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
} catch(Exception $e) {
	echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
	die();
}


$error = FALSE;
$errmsg = NULL;
$query = 'WITH Tmp(id, labl, addr, loc, phys, act) AS (VALUES';
$queryadd = "";
$querylink = "";
$addphys = 1;

if(isset($_POST) && !empty($_POST)){
	foreach ($_POST as $rowid => $tabval){
		//recup valeurs
		$myid = htmlspecialchars($rowid);
		if(isset($tabval["'label'"])){$mylabl = htmlspecialchars($tabval["'label'"]);}else{$mylabl = "";}
		if(isset($tabval["'address'"])){$myaddr = htmlspecialchars($tabval["'address'"]);}else{$myaddr = "";}
		if(isset($tabval["'localisation'"])){$myloc = htmlspecialchars($tabval["'localisation'"]);}else{$myloc = "";}
		if(isset($tabval["'physical'"])){$myphys = htmlspecialchars($tabval["'physical'"]);}else{$myphys = 0;}
		if(isset($tabval["'active'"])){$myact = htmlspecialchars($tabval["'active'"]);}else{$myact = 0;}
		
		//verif
		if($myphys == 1 && $myaddr == ""){$error = TRUE; $errmsg = 'Adresse requise si compteur physique';}
		if($myphys != 1 && $myaddr != ""){$error = TRUE; $errmsg = 'Adresse vide si compteur virtuel';}
		if($myphys != 1 && $myaddr == ""){$myaddr = 'NULL';}
		if(empty($myloc)){$myloc = NULL;}
		
		if($rowid == 'ajout'){
			if($mylabl != ""){
				$queryadd = 'INSERT INTO cpt_infos (label, address, localisation, physical, active) VALUES ';
				$queryadd .= '("'.$mylabl.'", '.$myaddr.', '.'"'.$myloc.'", '.$myphys.', '.$myact.')';
				if($myphys == 1){$addphys = 1;}else{$addphys = 0;}
			}
		} else {
			//verif
			if($mylabl == ""){$error = TRUE; $errmsg = $rowid.'____'.'Valeur "Label" requise';}
			//build requete
			$query .= '('.$myid.', "'.$mylabl.'", '.$myaddr.', '.'"'.$myloc.'", '.$myphys.', '.$myact.'),';
		}
	}
	if ($error) {
		echo "<p class='warningmsg'>".$errmsg."</p>";
	} else {
		//requete base
		$query = substr($query, 0, -1);
		$query .= ') UPDATE cpt_infos SET label = (SELECT labl FROM Tmp WHERE cpt_infos.id = Tmp.id),
			address = (SELECT addr FROM Tmp WHERE cpt_infos.id = Tmp.id),
			localisation = (SELECT loc FROM Tmp WHERE cpt_infos.id = Tmp.id),
			physical = (SELECT phys FROM Tmp WHERE cpt_infos.id = Tmp.id),
			active = (SELECT act FROM Tmp WHERE cpt_infos.id = Tmp.id)
			WHERE id IN (SELECT id FROM Tmp)';
		
		//modif cpt_links si phys/virt par trigger
		//TODO redirect vers virt ou cpt param
		$questmt = $pdo->prepare($query);
		$questmt->execute();
		
		if(!empty($queryadd)){
			$insstmt = $pdo->prepare($queryadd);
			try {
				$pdo->beginTransaction();
				$insstmt->execute();
				$lastinsert = $pdo->lastInsertId();
				//TODO : header vers virt ou cpt param
				//add entry cpt_links si physique par trigger
				$pdo->commit();
			} catch(Exception $e) {
				$pdo->rollBack();
				echo "Transaction failed : ".$e->getMessage();
				die();
			}
			//if($addphys){header('Location: cpt_param.php?cptchoice=');}else{header('Location: virt_param.php?cpt_choice=');}
		}
	}
}

$mycpt = $pdo->prepare('SELECT * FROM cpt_infos;');
$mycpt->execute();
$cpt_infos = $mycpt->fetchAll();

?>
		<header>
			<?php require_once("menu.php"); ?>
		</header>
		<div class="g-mask">.</div>
		<div id="content">
			<div id="intro">
				<h1>Liste des compteurs</h1>
			</div>
			<!--affichage tableau data-->
			<form id="SettingsDisplayForm" class="tableform" action="compteurs.php" method="post" accept-charset="utf-8">
				<table class="cpt_table">
					<thead>
						<th>Label</th>
						<th>Adresse</th>
						<th>Localisation</th>
						<th>Physique</th>
						<th>Actif</th>
						<th>Action</th>
					</thead>
					<tbody>
					<?php
						foreach ($cpt_infos as $row) {
							$checkphy = NULL;
							$checkact = NULL;
							if($row['physical'] == 1){$checkphy = 'checked';}
							if($row['active'] == 1){$checkact = 'checked';}
							
							echo '<tr>';
							echo '<td><input type="text" name="'.$row['id']."['label']".'" value="'.$row['label'].'"></td>';
							echo '<td><input type="text" name="'.$row['id']."['address']".'" value="'.$row['address'].'"></td>';
							echo '<td><input type="text" name="'.$row['id']."['localisation']".'" value="'.$row['localisation'].'"></td>';
							echo '<td><input type="checkbox" name="'.$row['id']."['physical']".'" value="1"'.$checkphy.'></td>';
							echo '<td><input type="checkbox" name="'.$row['id']."['active']".'" value="1"'.$checkact.'></td>';
							echo '<td>';
							if($row['physical'] == 1){
								echo '<a href="cpt_param.php?cptchoice='.$row['id'].'">Parametres</a>';
							}else{
								echo '<a href="virt_param.php?cptchoice='.$row['id'].'">Configuration</a>';
							};
							echo '</td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>
				<br/>
			<!--ajout nouvelle donnée-->
				<table class="cpt_table">
					<thead>
						<th>Label</th>
						<th>Adresse</th>
						<th>Localisation</th>
						<th>Physique</th>
						<th>Actif</th>
					</thead>
					<tbody>
							<tr>
							<td><input type="text" name="ajout['label']"></td>
							<td><input type="text" name="ajout['address']"></td>
							<td><input type="text" name="ajout['localisation']"></td>
							<td><input type="checkbox" name="ajout['physical']" value="1"></td>
							<td><input type="checkbox" name="ajout['active']" value="1"></td>
							</tr>
					</tbody>
				</table>
				<br/>
				<input value="Submit" type="submit">
			</form>
		</div>
		<?php require_once("footer.php"); ?>
	</body>
</html>


