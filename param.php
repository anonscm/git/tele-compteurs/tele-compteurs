<!DOCTYPE html>
<html>
	<head>
		<title>Parametres</title>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body>
	<header>
		<?php require_once("menu.php"); ?>
	</header>
	<div class="g-mask">.</div>
	<div id="content">
		<div class="g-around-flex">
			<div><img src="img/rasp_cpt_2.jpg" width="100%"></div>
			<div>
				<h1>Parametres</h1>
				<ul>
					<li class="menu"><a href="busconfig.php">Configuration Bus</a></li>
					<li class="menu"><a href="settings.php">Autres configurations</a></li>
					<li class="menu"><a href="datatypes.php">Types de donnees</a></li>
					<li class="menu"><a href="compteurs.php">Liste des compteurs</a></li>
				</ul>
			</div>
		</div>
	</div>
	<?php require_once("footer.php"); ?>
	</body>
</html>
