<!DOCTYPE html>
<html>
	<head>
		<title>Liste compteurs</title>
		<script src="ajax/dygraph.min.js"></script>
		<link rel="stylesheet" href="css/dygraph.css" />
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body>
<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

try{
	$pdo = new PDO('sqlite:'.dirname(__FILE__).'/compteur.db');
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
	//$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
} catch(Exception $e) {
	echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
	die();
}

$listcpt = $pdo->prepare('SELECT id, label FROM cpt_infos');
$listtype = $pdo->prepare('SELECT * FROM type_params');

$listcpt->execute();
$listtype->execute();

$listcpt_val = $listcpt->fetchAll(PDO::FETCH_ASSOC);
$listtype_val = $listtype->fetchAll(PDO::FETCH_ASSOC);

$choixcpt = array();
$choixtype = array();
$last_row = NULL;

?>
<?php
	//retrieve get parameters
	for($i = 1; $i <= 3; $i++){
		if(isset($_GET['cptchoice'.$i])){
			$choixcpt[$i] = (int)htmlspecialchars($_GET['cptchoice'.$i]);
		}
		if(isset($_GET['typechoice'.$i])){
			$choixtype[$i] = (int)htmlspecialchars($_GET['typechoice'.$i]);
		}
	}
	//build query
	$myquery = null;
	$validquery = 0;
	$validinput = array();
	$infosinput = array();
	$jslabel = '"Date,';
	$queryfields = '';
	$queryjoin = '';
	$querywhere = '';
	$results = null;
	for($i = 1; $i <= 3; $i++){
		if(($choixcpt != array()) && ($choixcpt[$i] != 0) && ($choixtype[$i] != 0)){
			$validquery = 1;
			$validinput[] = $i;
			$queryfields .= 'TR'.$i.'.val AS val'.$i.',';
			$queryjoin .= 'LEFT JOIN totalresus AS TR'.$i.' ON datasets.id=TR'.$i.'.dataset_id ';
			$querywhere .= 'TR'.$i.'.cptid='.$choixcpt[$i].' AND TR'.$i.'.type_id='.$choixtype[$i].' AND ';
		}
	}
	if($queryfields != ''){
		$queryfields = substr($queryfields, 0, -1);
		$querywhere = substr($querywhere, 0, -4);
		$myquery = 'SELECT datetime(datasets.date) AS dataset_date,'.$queryfields.' FROM datasets '.$queryjoin.' WHERE '.$querywhere.' ORDER BY dataset_date ASC;';
	}
	//print_r($myquery);
	if($validquery){
		$results = $pdo->prepare($myquery);
		$results->execute();
	}
?>
	<header>
		<?php require_once("menu.php"); ?>
	</header>
	<div class="g-mask">.</div>

	<div id="content">
		<div id="intro">
			<h1>Mesures compteur : Graph</h2>
			<a href="data_tab_cpt.php<?php $getstring='?';for($i = 1; $i <= 3; $i++){
				$getstring .= 'cptchoice'.$i.'='.$choixcpt[$i].'&typechoice'.$i.'='.$choixtype[$i].'&';} echo substr($getstring, 0, -1); ?>">
			<h2>Mode Tableau</h2></a>
			<form id="UserDisplayForm" action="data_cpt.php" class="flexcol" method="get" accept-charset="utf-8">
				<label>Choix des datas</label>
			<?php for ($i = 1; $i <= 3; $i++): //création menus déroulants et constructions tableaux d'infos sur la selection?>
				<div class="oneline">
					<select id="CptList<?php echo $i; ?>" class="listderoul" name="cptchoice<?php echo $i; ?>">
					<option value="0">--Choisir compteur--</option>
					<?php
						foreach ($listcpt_val as $row) {
							$myselected = '';
							if($choixcpt[$i] != 0 AND $choixcpt[$i] == $row['id']){
								$myselected = ' selected="selected"';
								$jslabel .= $row['label'];
								$infosinput[$i]["cpt_label"] = $row['label'];
							}
							echo '<option value="'.$row['id'].'"'.$myselected.'>'.$row['label'].'</option>';
						}
					?>
					</select>
					<select id="TypeList<?php echo $i; ?>" class="listderoul" name="typechoice<?php echo $i; ?>">
					<option value="0">--Tous les types--</option>
					<?php
						foreach ($listtype_val as $row) {
							$myselected = '';
							if($choixtype[$i] != 0 AND $choixtype[$i] == $row['id']){
								$myselected = ' selected="selected"';
								$jslabel .= '_'.$row['label'].',';
								$infosinput[$i]["val_label"] = $row['label'];
								$infosinput[$i]["unit"] = $row['unit'];
								$infosinput[$i]["abbreviation"] = $row['abbreviation'];
							}
							echo '<option value="'.$row['id'].'"'.$myselected.'>'.$row['label'].' ('.$row['abbreviation'].')</option>';
						}
					?>
					</select>
				</div>
			<?php endfor; ?>
				<input value="Submit" type="submit">
			</form>
		</div>
		<div id="graphdiv"></div>
		<script type="text/javascript">
		g = new Dygraph(
			document.getElementById("graphdiv"),
			<?php
			$jslabel = substr($jslabel, 0, -1);
			$jslabel .= '\n"';
			$jslabel = preg_replace('/\s+/', '_', $jslabel);
			echo $jslabel;
			while ($row = $results->fetch(PDO::FETCH_ASSOC)) {
				$mystring = ' + '."\n".'"'.$row['dataset_date'];
				foreach($validinput as $rank){$mystring .= ','.$row['val'.$rank];}
				$mystring .= '\n"';
				echo $mystring;
				$last_row = $row;
			}
			?>
			,{ digitsAfterDecimal: 2, maxNumberWidth: 9, axisLabelWidth: 100}
		);
		</script>
		<br/>
		<!--affichage dernier relevé-->
		<?php if(!empty($last_row)): ?>
		<table class="cpt_table">
			<thead>
				<th>Date</th>
				<th>Cpt Label</th>
				<th>Val Label</th>
				<th>Abbrev.</th>
				<th>Valeur</th>
				<th>Unite</th>
			</thead>
			<tbody>
				<?php foreach($validinput as $rank): ?>
				<tr>
					<td><?php echo $last_row['dataset_date']; ?></td>
					<td><?php echo $infosinput[$rank]['cpt_label']; ?></td>
					<td><?php echo $infosinput[$rank]['val_label']; ?></td>
					<td><?php echo $infosinput[$rank]['abbreviation']; ?></td>
					<td><?php echo $last_row['val'.$rank]; ?></td>
					<td><?php echo $infosinput[$rank]['unit']; ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php endif;?>
		<br/>
	</div>
	<?php require_once("footer.php"); ?>
	</body>
</html>
