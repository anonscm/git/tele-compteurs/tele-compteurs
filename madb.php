<?php
/**
 * Exemple simple qui étend la classe SQLite3 et change les paramètres
 * __construct, puis, utilise la méthode de connexion pour initialiser la
 * base de données.
 */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open('/var/www/html/madbtest.db');
    }
}

$db = new MyDB();

if(!$db){
  echo $db->lastErrorMsg();
} else {
  echo "Opened database successfully\n";
}


$results = $db->query('SELECT * FROM matable');
/*
var_dump($results->fetchArray());
*/
echo '#############';

echo '<table>';
while ($row = $results->fetchArray()) {
	echo '<tr>';
    /*var_dump($row);*/
    foreach ($row as $key=>$value) {
		echo '<td>'.$value.'</td>';
	}
	echo '</tr>';
}
echo '</table>';
?>
