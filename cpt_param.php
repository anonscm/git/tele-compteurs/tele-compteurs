<!DOCTYPE html>
<html>
	<head>
		<title>Parametres - setting</title>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body>
<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
class MyDB extends SQLite3{function __construct(){$this->open('compteur.db');}}
$db = new MyDB();
if(!$db){echo $db->lastErrorMsg();}

$error = FALSE;
$errmsg = NULL;
$choixcpt = NULL;

if(isset($_GET['cptchoice'])){$choixcpt = (int)htmlspecialchars($_GET['cptchoice']);} else {header('Location: compteurs.php');}


$query = 'WITH Tmp(id, cpt_id, reg_addr, type_id, corr, act, home) AS (VALUES';
$queryadd = "";

if(isset($_POST) && !empty($_POST)){
	//print_r($_POST);
	foreach ($_POST as $rowid => $tabval){
		//recup valeurs
		$myid = htmlspecialchars($rowid);
		if(isset($tabval["'reg_address'"])){$myregadd = htmlspecialchars($tabval["'reg_address'"]);}else{$myregadd = "";}
		if(isset($tabval["'correctif'"])){$mycorr = htmlspecialchars($tabval["'correctif'"]);}else{$mycorr = "";}
		if(isset($tabval["'active'"])){$myact = htmlspecialchars($tabval["'active'"]);}else{$myact = 0;}
		if(isset($tabval["'show_home'"])){$myhome = htmlspecialchars($tabval["'show_home'"]);}else{$myhome = 0;}
		if(isset($tabval["'type_id'"])){$mytype = htmlspecialchars($tabval["'type_id'"]);}else{$mytype = "";}
		
		if($rowid == 'ajout'){
			if($myregadd != ""){
				if($myregadd == "" || $mycorr == "" || $mytype == 0){
					$error = TRUE; $errmsg = 'Adresse, facteur et type requis!';
				} else {
					$queryadd = 'INSERT INTO cpt_params (cpt_id, reg_address, type_id, correctif, active, show_home) VALUES ';
					$queryadd .= '('.$choixcpt.', '.$myregadd.', '.$mytype.', '.$mycorr.', '.$myact.', '.$myhome.')';
				}
			}
		} else {
			//verif
			if($myregadd == "" || $mycorr == "" || $mytype == 0){$error = TRUE; $errmsg = 'Adresse, facteur et type requis!';}
			//build requete
			$query .= '('.$myid.', '.$choixcpt.', '.$myregadd.', '.$mytype.', '.$mycorr.', '.$myact.', '.$myhome.'),';
		}
	}
	if ($error) {
		echo "<p class='warningmsg'>".$errmsg."</p>";
	} else {
		//requete base
		$query = substr($query, 0, -1);
		$query .= ') UPDATE cpt_params SET cpt_id = (SELECT cpt_id FROM Tmp WHERE cpt_params.id = Tmp.id),
			reg_address = (SELECT reg_addr FROM Tmp WHERE cpt_params.id = Tmp.id),
			type_id = (SELECT type_id FROM Tmp WHERE cpt_params.id = Tmp.id),
			correctif = (SELECT corr FROM Tmp WHERE cpt_params.id = Tmp.id),
			active = (SELECT act FROM Tmp WHERE cpt_params.id = Tmp.id),
			show_home = (SELECT home FROM Tmp WHERE cpt_params.id = Tmp.id)
			WHERE id IN (SELECT id FROM Tmp)';
		$myupdate = $db->exec($query);
		if(!empty($queryadd)){$myadd = $db->exec($queryadd);}
	}
}
	$paramslist = $db->query('SELECT * FROM paramslist WHERE cpt_id ='.$choixcpt.';');
	$compteur_name = $db->query('SELECT label, localisation, physical FROM cpt_infos WHERE id = '.$choixcpt.';');
	$listtype = $db->query('SELECT * FROM type_params');
	$cur_cpt = $compteur_name->fetchArray();
	//verif existance compteur physique
	if($cur_cpt['physical'] == 0){header('Location: compteurs.php');}
?>
		<header>
			<?php require_once("menu.php"); ?>
		</header>
		<div class="g-mask">.</div>
		<div id="content">
			<div id="intro">
				<h1>Parametres Compteur <?php echo $cur_cpt['label'].' ('.$cur_cpt['localisation'].')' ?></h1>
			</div>
			<!--affichage tableau data-->
			<form id="SettingsDisplayForm" class="tableform" action="cpt_param.php?cptchoice=<?php echo $choixcpt; ?>" method="post" accept-charset="utf-8">
				<table class="cpt_table">
					<thead>
						<th>Adresse Registre</th>
						<th>Facteur Correctif</th>
						<th>Actif</th>
						<th>Affiche Home</th>
						<th>Type</th>
					</thead>
					<tbody>
<?php while ($row = $paramslist->fetchArray()):
		$checkhome = NULL;
		$checkact = NULL;
		if($row['active'] == 1){$checkact = 'checked';}
		if($row['show_home'] == 1){$checkhome = 'checked';}
		
?>
						<tr>
							<td><input type="text" name="<?php echo $row['param_id']; ?>['reg_address']" value="<?php echo $row['reg_address']; ?>"></td>
							<td><input type="text" name="<?php echo $row['param_id']; ?>['correctif']" value="<?php echo $row['correctif']; ?>"></td>
							<td><input type="checkbox" name="<?php echo $row['param_id']; ?>['active']" value="1" <?php echo $checkact; ?>></td>
							<td><input type="checkbox" name="<?php echo $row['param_id']; ?>['show_home']" value="1" <?php echo $checkhome; ?>></td>
							<td>
							<select class="listderoul" name="<?php echo $row['param_id'];?>['type_id']">
							<option value="0">--Tous les types--</option>
							<?php
								while ($rowtype = $listtype->fetchArray()):
									$myselected = '';
									if($row['type_id'] == $rowtype['id']){$myselected = ' selected="selected"';}
									echo '<option value="'.$rowtype['id'].'"'.$myselected.'>'.$rowtype['label'].' ('.$rowtype['abbreviation'].')</option>';
								endwhile;
							?>
							</select>
							</td>
						</tr>
<?php endwhile ?>
					</tbody>
				</table>
				<br/>
			<!--ajout nouvelle donnée-->
				<table class="cpt_table">
					<thead>
						<th>Adresse Registre</th>
						<th>Facteur Correctif</th>
						<th>Actif</th>
						<th>Affiche Home</th>
						<th>Type</th>
					</thead>
					<tbody>
							<tr>
							<td><input type="text" name="ajout['reg_address']"></td>
							<td><input type="text" name="ajout['correctif']"></td>
							<td><input type="checkbox" name="ajout['active']" value="1"></td>
							<td><input type="checkbox" name="ajout['show_home']" value="1"></td>
							<td>
							<select class="listderoul" name="ajout['type_id']">
							<option value="">--Tous les types--</option>
							<?php
								while ($rowtype = $listtype->fetchArray()):
									$myselected = '';
									if($row['type_id'] == $rowtype['id']){$myselected = ' selected="selected"';}
									echo '<option value="'.$rowtype['id'].'"'.$myselected.'>'.$rowtype['label'].' ('.$rowtype['abbreviation'].')</option>';
								endwhile;
							?>
							</select>
							</tr>
					</tbody>
				</table>
				<br/>
				<input value="Submit" type="submit">
			</form>
		</div>
		<?php require_once("footer.php"); ?>
	</body>
</html>


