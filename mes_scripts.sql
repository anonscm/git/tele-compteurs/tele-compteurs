SELECT resultats.dataset_id AS dataset_id, resultats.date AS dataset, resultats.type_id AS type_id,
SUM(resultats.data*cpt_links.factor) AS val
FROM resultats
INNER JOIN cpt_links ON cpt_links.cpt_id = 1 AND resultats.cpt_id = cpt_links.cpt_item
WHERE resultats.cpt_id IN
(SELECT cpt_links.cpt_item FROM cpt_links WHERE cpt_links.cpt_id = 1)
GROUP BY resultats.dataset_id, resultats.date, resultats.type_id
ORDER BY dataset_id, type_id ASC
;


			<table class="cpt_table">
				<thead>
					<th>Date</th>
					<th>Cpt Label</th>
					<th>Val Label</th>
					<th>Abbrev.</th>
					<th>Correctif</th>
					<th>Unite</th>
					<th>Valeur</th>
				</thead>
				<tbody>
			<?php
			while ($row = $results->fetchArray()) {
				echo '<tr>';
					echo '<td>'.$row['dataset_date'].'</td>';
					echo '<td>'.$row['cpt_label'].'</td>';
					echo '<td>'.$row['val_label'].'</td>';
					echo '<td>'.$row['abbreviation'].'</td>';
					echo '<td>'.$row['correctif'].'</td>';
					echo '<td>'.$row['unit'].'</td>';
					echo '<td>'.$row['val'].'</td>';
				echo '</tr>';
			}
			?>
