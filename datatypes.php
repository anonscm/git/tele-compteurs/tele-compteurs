<!DOCTYPE html>
<html>
	<head>
		<title>Parametres - Setting</title>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body>
<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
class MyDB extends SQLite3{function __construct(){$this->open('compteur.db');}}
$db = new MyDB();
if(!$db){echo $db->lastErrorMsg();}

$error = FALSE;
$errmsg = NULL;
$query = 'WITH Tmp(id, label, unit, abbreviation) AS (VALUES';
$queryadd = "";

if(isset($_POST) && !empty($_POST)){
	foreach ($_POST as $rowid => $tabval){
		//recup valeurs
		$myid = htmlspecialchars($rowid);
		if(isset($tabval["'label'"])){$mylabl = htmlspecialchars($tabval["'label'"]);}else{$mylabl = "";}
		if(isset($tabval["'unit'"])){$myunit = htmlspecialchars($tabval["'unit'"]);}else{$myunit = "";}
		if(isset($tabval["'abbreviation'"])){$myabb = htmlspecialchars($tabval["'abbreviation'"]);}else{$myabb = "";}
		
		if($rowid == 'ajout'){
			if($mylabl != "" || $myunit != "" || $myabb != ""){
				if($mylabl == "" || $myunit == "" || $myabb == ""){
					$error = TRUE;
					$errmsg = $rowid.'____'.'Tous les champs sont obligatoire.';
				} else {
					$queryadd = 'INSERT INTO type_params (label, unit, abbreviation) VALUES ';
					$queryadd .= '("'.$mylabl.'", "'.$myunit.'", "'.$myabb.'");';
				}
			}
		} else {
			if($mylabl == "" || $myunit == "" || $myabb == ""){
				$error = TRUE;
				$errmsg = $rowid.'____'.'Tous les champs sont obligatoire.';
			} else {
				//build requete
				$query .= '('.$myid.', "'.$mylabl.'", "'.$myunit.'", "'.$myabb.'"),';
			}
		}
	}
	if ($error) {
		echo "<p class='warningmsg'>".$errmsg."</p>";
	} else {
		//requete base
		$query = substr($query, 0, -1);
		$query .= ') UPDATE type_params SET label = (SELECT label FROM Tmp WHERE type_params.id = Tmp.id),
			unit = (SELECT unit FROM Tmp WHERE type_params.id = Tmp.id),
			abbreviation = (SELECT abbreviation FROM Tmp WHERE type_params.id = Tmp.id)
			WHERE id IN (SELECT id FROM Tmp);';
		$myupdate = $db->exec($query);
		if(!empty($queryadd)){$myadd = $db->exec($queryadd);}
	}
}
	$type_params = $db->query('SELECT * FROM type_params');
?>
		<header>
			<?php require_once("menu.php"); ?>
		</header>
		<div class="g-mask">.</div>
		<div id="content">
			<div id="intro">
				<h1>Types de donnees</h1>
			</div>
			<!--affichage tableau data-->
			<form id="SettingsDisplayForm" class="tableform" action="datatypes.php" method="post" accept-charset="utf-8">
				<table class="cpt_table">
					<thead>
						<th>Label</th>
						<th>Unité</th>
						<th>Abreviation</th>
					</thead>
					<tbody>
					<?php
						while ($row = $type_params->fetchArray()) {
							echo '<tr>';
							echo '<td><input type="text" name="'.$row['id']."['label']".'" value="'.$row['label'].'"></td>';
							echo '<td><input type="text" name="'.$row['id']."['unit']".'" value="'.$row['unit'].'"></td>';
							echo '<td><input type="text" name="'.$row['id']."['abbreviation']".'" value="'.$row['abbreviation'].'"></td>';
							echo '</tr>';
						}
					?>
					</tbody>
				</table>
				<br/>
			<!--ajout nouvelle donnée-->
				<table class="cpt_table">
					<thead>
						<th>Label</th>
						<th>Unité</th>
						<th>Abreviation</th>
					</thead>
					<tbody>
							<tr>
							<td><input type="text" name="ajout['label']"></td>
							<td><input type="text" name="ajout['unit']"></td>
							<td><input type="text" name="ajout['abbreviation']"></td>
							</tr>
					</tbody>
				</table>
				<input value="Submit" type="submit">
			</form>
		</div>
		<?php require_once("footer.php"); ?>
	</body>
</html>



