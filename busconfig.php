<!DOCTYPE html>
<html>
	<head>
		<title>Parametres - Bus</title>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body>
<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
class MyDB extends SQLite3{function __construct(){$this->open('compteur.db');}}
$db = new MyDB();
if(!$db){echo $db->lastErrorMsg();}

$error = FALSE;
$query = 'WITH Tmp(id, valeur) AS (VALUES';

if(isset($_POST) && !empty($_POST)){
	foreach ($_POST as $rowid => $valeur){
		$myid = htmlspecialchars($rowid);
		$myvaleur = htmlspecialchars($valeur);
		if(ctype_digit($myid) && ctype_digit($myvaleur) && isset($myvaleur)){
			$query .= '('.$myid.', '.$myvaleur.'),';
		} else {
			$error = TRUE;
			print_r($myvaleur);
		}
	}
	if ($error) {
		echo "<p class='warningmsg'>Merci de saisir un nombre entier dans le formulaire</p>";
	} else {
		$query = substr($query, 0, -1);
		$query .= ') UPDATE busconfig SET busvalue = (SELECT valeur FROM Tmp WHERE busconfig.id = Tmp.id) WHERE id IN (SELECT id FROM Tmp)';
		$myupdate = $db->query($query);
	}
}

$busconfig = $db->query('SELECT id, busparam, busvalue FROM busconfig');
?>
		<header>
			<?php require_once("menu.php"); ?>
		</header>
		<div class="g-mask">.</div>
		
		<div id="content">
			<div id="intro">
				<h1>Configuration du Bus</h1>
			</div>
			<!--affichage tableau data-->
			<form id="BusDisplayForm" class="tableform" action="busconfig.php" method="post" accept-charset="utf-8">
				<table class="cpt_table">
					<thead>
						<th>Parametre</th>
						<th>Valeur</th>
					</thead>
					<tbody>
				<?php
					while ($row = $busconfig->fetchArray()) {
						echo '<tr>';
							echo '<td>'.$row['busparam'].'</td>';
							echo '<td><input type="text" name="'.$row['id'].'" value="'.$row['busvalue'].'"></td>';
						echo '</tr>';
					}
				?>
					</tbody>
				</table>
				<br/>
				<input value="Submit" type="submit">
			</form>
		</div>
		<?php require_once("footer.php"); ?>
	</body>
</html>


