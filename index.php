<!DOCTYPE html>
<html>
<?php
try{
	$pdo = new PDO('sqlite:'.dirname(__FILE__).'/compteur.db');
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
} catch(Exception $e) {
	echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
	die();
}
try {
	$myst = $pdo->prepare('SELECT valeur FROM setting WHERE setting.description = "TABNAME"' );
	$myst->execute();
	$tabname = $myst->fetch();
} catch(PDOException $e) {
	echo "An error occured reading setting table!"; 
	echo $e->getMessage();                   
}
try {
	$mylast = $pdo->prepare('SELECT * FROM show_resu');
	$mylast->execute();
	$lastval = $mylast->fetchall();
} catch(PDOException $e) {
	echo "An error occured reading setting table!"; 
	echo $e->getMessage();                   
}
?>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="css/style.css" />
</head>
<body>
	<header>
		<?php require_once("menu.php"); ?>
	</header>
    <div id="content">
		<div class="full-width">
			<img class="full-width" src="img/panoralarger.jpg">
			<div class="maskabsolute">
				<div id="fronttitle">Telereleve Des Compteurs Electriques : <?php echo $tabname['valeur'] ?></div>
			</div>
		</div>
		<div class="show-index">
		<?php
			foreach ($lastval as $cindex):
				echo '<div class="g-index"><h1>'.$cindex['cpt_label'].'</h1><h2>'.$cindex['val_label'].' ('.$cindex['abbreviation'].
				')</h2><div class="v-index">'.$cindex['data']*$cindex['correctif'].
				'<span class="u-index">'.$cindex['unit'].'</span></div></div>';
			endforeach;
		?>
		</div>
	</div>
	<?php require_once("footer.php"); ?>
</body>
</html>

