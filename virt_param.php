<!DOCTYPE html>
<html>
<?php
try{
	$pdo = new PDO('sqlite:'.dirname(__FILE__).'/compteur.db');
	//$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
	//$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
} catch(Exception $e) {
	echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
	die();
}
?>
<?php
$error = FALSE;
$errmsg = NULL;
$choixcpt = NULL;

if(isset($_GET['cptchoice'])){$choixcpt = (int)htmlspecialchars($_GET['cptchoice']);} else {header('Location: compteurs.php');}
$myst = $pdo->prepare('SELECT label, localisation, physical FROM cpt_infos WHERE id = :cptchoix' );
$myst->bindParam(':cptchoix', $choixcpt, PDO::PARAM_INT);
$myst->execute();
$cur_cpt = $myst->fetch();
if($cur_cpt['physical'] == 1){header('Location: compteurs.php');};

$mycpt = $pdo->prepare('SELECT * FROM cpt_infos;');
$mycpt->execute();
$cpt_infos = $mycpt->fetchAll();

?>
<?php
$query = 'WITH Tmp(id, cpt_id, cpt_item, factor) AS (VALUES ';
$querydel = "";
$listdel = "";
if(isset($_POST) && !empty($_POST)){
	//print_r($_POST);
	foreach ($_POST as $rowid => $tabval){
		//recup valeurs
		$myid = htmlspecialchars($rowid);
		if(isset($tabval["'cpt_id'"])){$mycptid = htmlspecialchars($tabval["'cpt_id'"]);}else{$mycptid = "";}
		if(isset($tabval["'cpt_item'"])){$mycptitem = htmlspecialchars($tabval["'cpt_item'"]);}else{$mycptitem = "";}
		if(isset($tabval["'factor'"])){$myfactor = htmlspecialchars($tabval["'factor'"]);}else{$myfactor = "";}
		if(isset($tabval["'suppr'"])){$mysuppr = htmlspecialchars($tabval["'suppr'"]);}else{$mysuppr = "";}

		if($rowid == 'ajout'){
			if($mycptitem != ""){
				if($myfactor == "" || $mycptid == ""){
					$error = TRUE; $errmsg = 'Compteur et multiplicateur requis!';
				} else {
					$queryadd = 'INSERT INTO cpt_links (cpt_id, cpt_item, factor) VALUES ';
					$queryadd .= '('.$mycptid.', '.$mycptitem.', '.$myfactor.')';
				}
			}
		} else {
			//verif
			if($mycptid == "" || $mycptitem == "" || $myfactor == ""){$error = TRUE; $errmsg = 'Compteur et multiplicateur requis!';}
			//build requete
			$query .= '('.$myid.', '.$mycptid.', '.$mycptitem.', '.$myfactor.'),';
			if($mysuppr == 1){$listdel .= $myid.',';}
		}
	}
	if ($error) {
		echo "<p class='warningmsg'>".$errmsg."</p>";
	} else {
		//requete base
		$query = substr($query, 0, -1);
		$query .= ') UPDATE cpt_links SET cpt_id = (SELECT cpt_id FROM Tmp WHERE cpt_links.id = Tmp.id),
			cpt_item = (SELECT cpt_item FROM Tmp WHERE cpt_links.id = Tmp.id),
			factor = (SELECT factor FROM Tmp WHERE cpt_links.id = Tmp.id)
			WHERE id IN (SELECT id FROM Tmp)';
			
		if(!empty($queryadd)){
			try{
				//print_r($queryadd);
				$insstmt = $pdo->prepare($queryadd);
				$insstmt->execute();
			} catch(PDOException $e) {
				echo "An error occured inserting data in table cpt_links!"; 
				echo $e->getMessage();                   
			}
		}
		try{
			$updstmt = $pdo->prepare($query);
			$updstmt->execute();
		} catch(PDOException $e) {
			echo "An error occured updating table cpt_links!"; 
			echo $e->getMessage();                
		}
		if(!empty($listdel)){
			try{
				$querydel = 'DELETE FROM cpt_links WHERE id IN ('.substr($listdel, 0, -1).')';
				$delstmt = $pdo->prepare($querydel);
				$delstmt->execute();
			} catch(PDOException $e) {
				echo "An error occured deleting data in table cpt_links!"; 
				echo $e->getMessage();
			}
		}
	}
}

try {

	$stmt = $pdo->prepare('SELECT * FROM cpt_links WHERE cpt_id = :cptchoix');
	$stmt->bindParam(':cptchoix', $choixcpt, PDO::PARAM_INT);
	$stmt->execute();

	$links_list = $stmt->fetchAll();
	//$stmt->debugDumpParams();
	//print_r($links_list);
} catch(PDOException $e) {
	echo "An error occured reading cpt_links table!"; 
	echo $e->getMessage();                   
}

?>
	<head>
		<title>Compteurs - setting</title>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body>
		<header>
			<?php require_once("menu.php"); ?>
		</header>
		<div class="g-mask">.</div>
		<div id="content">
			<div id="intro">
				<h1>Configuration compteurs virtuels : <?php echo $cur_cpt['label'].' ('.$cur_cpt['localisation'].')' ?></h1>
			</div>
			<!--affichage tableau data-->
			<form id="SettingsDisplayForm" class="tableform" action="virt_param.php?cptchoice=<?php echo $choixcpt; ?>" method="post" accept-charset="utf-8">
				<table class="cpt_table">
					<thead>
						<th>Compteur Physique</th>
						<th>Multiplicateur (ex: 1, -1 ...)</th>
						<th>Supprimer</th>
					</thead>
					<tbody>
<?php foreach ($links_list as $row):?>
						<tr>
							<input type="hidden" name="<?php echo $row['id']; ?>['cpt_id']" value="<?php echo $row['cpt_id']; ?>">
							<td>
							<select class="listderoul" name="<?php echo $row['id'];?>['cpt_item']">
							<option value="0">--Compteurs physiques--</option>
							<?php
								foreach ($cpt_infos as $rowcpt):
									$myselected = '';
									if($row['cpt_item'] == $rowcpt['id']){$myselected = ' selected="selected"';}
									echo '<option value="'.$rowcpt['id'].'"'.$myselected.'>'.$rowcpt['label'].' ('.$rowcpt['localisation'].')</option>';
								endforeach;
							?>
							</select>
							</td>
							<td><input type="text" name="<?php echo $row['id']; ?>['factor']" value="<?php echo $row['factor']; ?>"></td>
							<td><input type="checkbox" name="<?php echo $row['id']; ?>['suppr']" value="1"></td>
						</tr>
<?php endforeach ?>
					</tbody>
				</table>
				<br/>
				<p>Ajout nouvelle opération :</p>
			<!--ajout nouvelle donnée-->
				<table class="cpt_table">
					<thead>
						<th>Compteur Physique</th>
						<th>Multiplicateur (ex: 1, -1 ...)</th>
					</thead>
					<tbody>
							<tr>
								<input type="hidden" name="ajout['cpt_id']" value="<?php echo $choixcpt; ?>">
								<td>
								<select class="listderoul" name="ajout['cpt_item']">
								<option value="">--Compteurs physiques--</option>
								<?php
									foreach ($cpt_infos as $rowcpt):
										$myselected = '';
										echo '<option value="'.$rowcpt['id'].'"'.$myselected.'>'.$rowcpt['label'].' ('.$rowcpt['localisation'].')</option>';
									endforeach;
								?>
								</select>
								</td>
								<td><input type="text" name="ajout['factor']"></td>
							</tr>
					</tbody>
				</table>
				<br/>
				<input value="Submit" type="submit">
			</form>
		</div>
		<?php require_once("footer.php"); ?>
</body>
</html>
