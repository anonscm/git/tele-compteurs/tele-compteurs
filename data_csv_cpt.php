<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
try{
	$pdo = new PDO('sqlite:'.dirname(__FILE__).'/compteur.db');
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); // ERRMODE_WARNING | ERRMODE_EXCEPTION | ERRMODE_SILENT
	//$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
} catch(Exception $e) {
	echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
	die();
}
?>
<?php
	//retrieve get parameters
	for($i = 1; $i <= 3; $i++){
		if(isset($_GET['cptchoice'.$i])){
			$choixcpt[$i] = (int)htmlspecialchars($_GET['cptchoice'.$i]);
		}
		if(isset($_GET['typechoice'.$i])){
			$choixtype[$i] = (int)htmlspecialchars($_GET['typechoice'.$i]);
		}
	}
	
	//retrieve cpt and type
	$listcpt_label = array();
	$listtype_label = array();
	$listcpt = $pdo->prepare('SELECT id, label FROM cpt_infos');
	$listtype = $pdo->prepare('SELECT * FROM type_params');
	$listcpt->execute();
	$listtype->execute();
	while($row = $listcpt->fetch(PDO::FETCH_ASSOC)){$listcpt_label[$row['id']] = $row['label'];}
	while($row = $listtype->fetch(PDO::FETCH_ASSOC)){$listtype_label[$row['id']] = $row['label'];}
	
	//build query
	$myquery = null;
	$validquery = 0;
	$validinput = array();
	$infosinput = array();
	$queryfields = '';
	$querywhere = '';
	$results = null;

	for($i = 1; $i <= 3; $i++){
		if(($choixcpt != array()) && ($choixcpt[$i] != 0) && ($choixtype[$i] != 0)){
			$validquery = 1;
			$validinput[] = $i;
			$queryfields = '*';
			$querywhere .= '(cptid='.$choixcpt[$i].' AND type_id='.$choixtype[$i].') OR ';
		}
	}
	if($queryfields != ''){
		$querywhere = substr($querywhere, 0, -3);
		$myquery = 'SELECT '.$queryfields.' FROM totalresus WHERE '.$querywhere.' ORDER BY dataset_date, cpt_label, val_label DESC;';
	}

	if($validquery){
		$results = $pdo->prepare($myquery);
		$results->execute();
	}
?>
<?php
//filename
$filename = date('Ymd').'_';
foreach($validinput AS $rank){
	$filename .= $listcpt_label[$choixcpt[$rank]].'_'.$listtype_label[$choixcpt[$rank]].'_';
}
$filename .= '.csv';
$filename = preg_replace('/\s+/', '_', $filename);
//content
header("Content-Type: text/plain");
header("Content-disposition: attachment; filename=$filename");
$out = fopen('PHP://output', 'w');

while ($row = $results->fetch(PDO::FETCH_ASSOC)){
	fputcsv($out, array(
		$row['dataset_date'],
		$row['cpt_label'],
		$row['val_label'],
		$row['abbreviation'],
		$row['val'],
		$row['unit']
	));
}
fclose($out);
?>
