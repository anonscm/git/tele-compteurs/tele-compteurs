#!/usr/bin/python3
#coding: utf8

import sys
import minimalmodbus
import serial
import time
import binascii
#import pyModbusTCP
import sqlite3

#""""""""""""""""""""""""""""""""""""Setting Config""""""""""""""""""""""""""""""
def USBConfig():
    setting = db.cursor()
    setting.execute("Select * from setting WHERE description = 'USB' ")
    setting_data = setting.fetchone()
    USBset = setting_data[2]
    USBset = "{0}".format(USBset)
    return USBset
    

#"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
try:
    db = sqlite3.connect('/home/pi/Documents/socomec/compteur.db')
    busconfig = db.cursor()
    tasklist = db.cursor()
    records = db.cursor()
    datasets = db.cursor()

    busconfig.execute("Select * from busconfig")
    tasklist.execute("Select * from tasklist")

    busconfig_data = busconfig.fetchall()
    tasklist_data = tasklist.fetchall()
except Exception as e:
    sys.stderr.write('Database file not found or corrupted!')

#"""""""""""""""""""""""""""""""Load Bus Config""""""""""""""""""""""""""""""""""
def loadBusConfig(busconfig_data):
    for row in busconfig_data:
        if row[1] == 'baudrate':
            Compteur.serial.baudrate = row[2]
        elif row[1] == 'bytesize':
            Compteur.serial.bytesize = row[2]
        elif row[1] == 'parity':
            if row[2] == 0 :
                Compteur.serial.parity = serial.PARITY_NONE
            else :
                Compteur.serial.parity = serial.PARITY_EVEN            
        elif row[1] == 'stopbits':
                Compteur.serial.stopbits = row[2]
        elif row[1] == 'timeout' :
                Compteur.serial.timeout = row[2]
        elif row[1] == 'localecho':
            if row[2] == 0 :
                Compteur.handle_local_echo = False
            else :
                Compteur.handle_local_echo = True              
        elif row[1] == 'debug':
            if row[2] == 0 :
                Compteur.debug = False
            else :
                Compteur.debug = True

#""""""""""""""""""""""""""""""""""""Print Bus Config""""""""""""""""""""""""""""
def printBusConfig():
    print ("____config____")
    print ("baudrate : {0:d}".format(Compteur.serial.baudrate))
    print ("bytesize : {0:d}".format(Compteur.serial.bytesize))
    print ("stopbits : {0:d}".format(Compteur.serial.stopbits))
    print ("timeout  : {0:g}ms".format(1000*Compteur.serial.timeout))
    print ("echo : {0}".format(Compteur.handle_local_echo))
    print('____serial data____')

#""""""""""""""""""""""""""""""""""""USB device connexion""""""""""""""""""""""""

try:
    USBpath = USBConfig()
    COM_Port = serial.Serial(USBpath)
    COM_Port.setRTS(1)
    COM_Port.setDTR(1)
    pBC = 0; #printBusConfig done   
except OSError :
        sys.stderr.write("IOError check USB device")
#"""""""""""""""""""""""""""""""Initialisation ComRS485"""""""""""""""""""""""""
else:
    try :
        datasets.execute("INSERT INTO datasets (date) VALUES(STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW'))");
        datasets_lastid = datasets.lastrowid
        for row in tasklist_data:
            myrawvalue = 0;
            try :
                Compteur = minimalmodbus.Instrument(USBpath,row[1])
                loadBusConfig(busconfig_data)
                if pBC == 0:    #print Bus Config
                    printBusConfig()
                    pBC = 1
                myrawvalue = Compteur.read_long(row[2],3,False)
                myvalue =  myrawvalue * row[7]
            except OSError :
                sys.stderr.write('IOError %s %s %s' % (row[0],row[1],row[2]))
                continue
            except ValueError :
                sys.stderr.write('Value Error %s %s %s' % (row[0],row[1],row[2]))
                continue
            except NameError :
                sys.stderr.write('Name Error %s %s %s' % (row[0],row[1],row[2]))
                continue

        #"""""""""""""""""""""""""""""""Results Data"""""""""""""""""""""""""""""""""""""""""
            else :
                #print ("{0}: {1} Bin: {2:08b}| {1} Hex: 0x{2:08X}| {1} Déc {3:08f} {4} ".format(row[4],row[5],myrawvalue, myvalue, row[6]))
                records.execute("INSERT INTO records (data, date, dataset_id, param_id) VALUES(?,STRFTIME('%Y-%m-%d %H:%M:%f', 'NOW'),?,?)",(myrawvalue,datasets_lastid,row[3]))
        db.commit()
    except Exception as e:
        # Roll back any change if something goes wrong
        db.rollback()
        raise e
    finally :
        db.close()

       
    

