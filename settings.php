<!DOCTYPE html>
<html>
	<head>
		<title>Parametres - Setting</title>
		<link rel="stylesheet" href="css/style.css" />
	</head>
	<body>
<?php
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
class MyDB extends SQLite3{function __construct(){$this->open('compteur.db');}}
$db = new MyDB();
if(!$db){echo $db->lastErrorMsg();}

$error = FALSE;
$query = 'WITH Tmp(id, val) AS (VALUES';

if(isset($_POST) && !empty($_POST)){
	foreach ($_POST as $rowid => $valeur){
		$myid = htmlspecialchars($rowid);
		$myvaleur = htmlspecialchars($valeur);
		if(empty($myvaleur)){
			$error = TRUE;
		} else {
			$query .= '('.$myid.', "'.$myvaleur.'"),';
		}
	}
	if ($error) {
		echo "<p class='warningmsg'>LEs valeurs vides ne sont pas autorisées dans le formulaire</p>";
	} else {
		$query = substr($query, 0, -1);
		$query .= ') UPDATE setting SET valeur = (SELECT val FROM Tmp WHERE setting.id = Tmp.id) WHERE id IN (SELECT id FROM Tmp)';
		$myupdate = $db->exec($query);
	}
}

$busconfig = $db->query('SELECT id, description, valeur FROM setting');
?>
		<header>
			<?php require_once("menu.php"); ?>
		</header>
		<div class="g-mask">.</div>
		<div id="content">
			<div id="intro">
				<h1>Autres Configurations</h1>
			</div>
			<!--affichage tableau data-->
			<form id="SettingsDisplayForm" class="tableform" action="settings.php" method="post" accept-charset="utf-8">
				<table class="cpt_table">
					<thead>
						<th>Description</th>
						<th>Valeur</th>
					</thead>
					<tbody>
				<?php
					while ($row = $busconfig->fetchArray()) {
						echo '<tr>';
							echo '<td>'.$row['description'].'</td>';
							echo '<td><input type="text" name="'.$row['id'].'" value="'.$row['valeur'].'"></td>';
						echo '</tr>';
					}
				?>
					</tbody>
				</table>
				<br/>
				<input value="Submit" type="submit">
			</form>
		</div>
		<?php require_once("footer.php"); ?>
	</body>
</html>


